{-# LANGUAGE ScopedTypeVariables #-}
import System.Process

import System.Taffybar

import System.Taffybar.Systray
import System.Taffybar.SimpleClock
import System.Taffybar.FSMonitor
import System.Taffybar.Battery
import System.Taffybar.NetMonitor
import System.Taffybar.Pager
import System.Taffybar.TaffyPager

import System.Taffybar.Widgets.PollingBar
import System.Taffybar.Widgets.PollingGraph
import System.Taffybar.Widgets.PollingLabel
import System.Taffybar.Widgets.PollingLabel

import System.Information.Memory
import System.Information.CPU
import System.Information.Battery



import qualified Data.Time.Clock as Clock

import Graphics.UI.Gtk
import Graphics.UI.Gtk.Display.Image
import Graphics.UI.Gtk.Layout.HBox

import Data.Time.Format
import Data.Time.LocalTime


memCfg = defaultGraphConfig
  { graphDataColors = [(22/255, 147/255, 165/255, 1)]
  , graphLabel = Nothing
  , graphWidth = 40
  , graphPadding = 3
  , graphBorderColor = (0.247, 0.247, 0.247)
  }

cpuCfg = defaultGraphConfig
  { graphDataColors = [(251/255, 184/255, 41/255, 1), (1, 0, 1, 0.5)]
  , graphLabel = Nothing
  , graphWidth = 40
  , graphPadding = 3
  , graphBorderColor = (0.247, 0.247, 0.247)
  }

batCfg = defaultBatteryConfig
  { barPadding     = 3
  , barColor       = \perc -> if perc < 0.1 then (1,0,0) else if perc < 0.3 then (1,0.75,0) else (0.1,0.75,0)
  , barBorderColor = (0.247, 0.247, 0.247)
  , barWidth       = 14
  }

volumeCfg = (defaultBarConfig $ \perc -> if perc > 0.8 then (1,0,0) else (1,1,1))
  { barPadding     = 3
  , barBorderColor = (0.247, 0.247, 0.247)
  , barWidth       = 14
  }
memCallback = do
  mi <- parseMeminfo
  return [memoryUsedRatio mi]

cpuCallback = do
  (userLoad, systemLoad, totalLoad) <- cpuLoad
  return [totalLoad, systemLoad]

volumeCallback = do
  amixer <- readProcess "amixer" ["sget", "Master"] ""
  lin <- readProcess "awk" ["-F", "[[%]", "/%/ { print $2 }"] amixer
  let [l :: Int, r :: Int] = read <$> lines lin
  return $ (fromIntegral $ l + r) / 200
  
main = do
  let pager = taffyPagerNew defaultPagerConfig
      clock = textClockNew Nothing "%a %b %_d %H:%M" 1
      tray = systrayNew
      cpu = pollingGraphNew cpuCfg 0.5 cpuCallback
      mem = pollingGraphNew memCfg 0.5 memCallback
      bat = batteryBarNew batCfg 5
      battime = textBatteryNew "$time$" 5
      net = netMonitorNew 0.5 "wlp4s0"
      disk = fsMonitorNew 500 ["/"]
      volume = pollingBarNew volumeCfg 5 volumeCallback
  defaultTaffybar defaultTaffybarConfig { startWidgets = [ pager, net, bat, battime ]
                                        , endWidgets = [ tray, clock, cpu, mem, disk, volume ]
                                        }
