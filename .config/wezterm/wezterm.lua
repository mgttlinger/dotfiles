return {
  color_scheme = "Afterglow",
  default_prog = {"/usr/bin/env", "nu"},
  enable_kitty_keyboard = true,
  hide_tab_bar_if_only_one_tab = true,
  front_end = "WebGpu",
}
