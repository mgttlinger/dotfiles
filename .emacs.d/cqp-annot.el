 ;;; cqp-annot.el --- Minor mode for annotating match regions

;; Copyright (C) 2021 Merlin Göttlinger

;; Author: Merlin Göttlinger
;; Keywords: extensions

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(defun cqp-annot-wrap (label)
  "Wrap the selectin in annotation tag"
  (interactive "M")
  (let ((before (concat "<" label ">"))
        (after (concat "</" label ">")))
    (save-excursion
      (cond ((> (mark) (point))
             (insert before)
             (goto-char (mark))
             (insert after))
            (t
             (insert after)
             (goto-char (mark))
             (insert before))))))

(defvar cqp-annot-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c RET") 'cqp-annot-wrap)
    (mapc (lambda (n) (progn (define-key map (kbd (concat "C-c " n)) `(lambda () (interactive) (cqp-annot-wrap ,n))) (define-key map (kbd (concat "C-c C-" n)) `(lambda () (interactive) (cqp-annot-wrap (concat ,n "'")))))) (list "0" "1" "2" "3" "4" "5" "6" "7" "8" "9"))
    map)
  "Keymap for `cqp-annot-mode'.")


(defvar cqp-annot-font-lock-keywords
  `(( "<\\([^>]+\\)>\\([^<]*\\)</\\(\\1\\)>" (1 font-lock-variable-name-face) (2 font-lock-keyword-face) (3 font-lock-variable-name-face))
    )
  "Keyword highlighting specification for `cqp-annot-mode'.")

;;;###autoload
(define-minor-mode cqp-annot-mode
    "A minor mode for annotating regions."
  :init-value nil
  :lighter "cqp-annot"
  :keymap cqp-annot-mode-map
  (font-lock-add-keywords nil cqp-annot-font-lock-keywords)
  (font-lock-fontify-buffer))

(provide 'cqp-annot)
 ;;; cqp-annot.el ends here
