 ;;; cqp.el --- Major mode for editing CQP queries

;; Copyright (C) 2019 Merlin Göttlinger

;; Author: Merlin Göttlinger
;; Keywords: extensions

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(defvar cqp-cip-machine "clue21.linguistik.uni-erlangen.de" "The machine to use for executing CQP queries.")
(defvar cqp-cip-user "yg63ukub" "The user to use for executing CQP queries.")

(defun cqp-get-cached-result ()
  "Connects to the CIP machine and displays te cached result of the query."
  (interactive)
  (split-window)
  (find-file (s-concat "/ssh:" cqp-cip-user "@" cqp-cip-machine ":~/CQP/brexit_macros/" (buffer-name) "#*.cash") t))

(defun cqp-run-query ()
  "Connects to the CIP machine and displays te cached result of the query."
  (interactive)
  (let ((bn "*cqp*")
        (cmd (s-concat "ssh " cqp-cip-user "@" cqp-cip-machine " \"~/CQP/brexit_macros/run " (buffer-name) "\"")))
    (with-output-to-temp-buffer bn (async-shell-command cmd bn))))


(defun cqp-update-cached-results ()
  "Connects to the CIP machine and runs the update script to generate new caches."
  (interactive)
  (let ((bn "*cqp*")
        (cmd (s-concat "ssh " cqp-cip-user "@" cqp-cip-machine " \"~/CQP/brexit_macros/update\"")))
    (with-output-to-temp-buffer bn (async-shell-command cmd bn))))

(defvar cqp-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-d") #'cqp-run-query)
    (define-key map (kbd "C-c C-c") #'cqp-get-cached-result)
    (define-key map (kbd "C-c C-u") #'cqp-update-cached-results)
    map)
  "Keymap for `cqp-mode'.")

(defvar cqp-keywords
  '("define"
    "within"))

(defvar cqp-builtins
  '("macro"))

(defvar cqp-mode-syntax-table
  (let ((st (make-syntax-table prog-mode-syntax-table)))
    (modify-syntax-entry ?# "<" st)
    (modify-syntax-entry ?\n ">" st)
    (modify-syntax-entry ?$ "_" st)
    (modify-syntax-entry ?\[ "(]" st)
    (modify-syntax-entry ?\] ")[" st)
    (modify-syntax-entry ?\( "()" st)
    (modify-syntax-entry ?\) ")(" st)
    (modify-syntax-entry ?\" "\"" st)
    (modify-syntax-entry ?= "." st)
    (modify-syntax-entry ?| "." st)
    st)
  "Syntax table for `cqp-mode'.")

(defvar cqp-font-lock-keywords
  `(( ,(rx "@" (1+ digit)) . font-lock-variable-name-face)
    ( ,(regexp-opt '("+" "?" "*" "!=" "=" "|") t) . font-lock-keyword-face)
    ( ,(regexp-opt cqp-keywords 'words) . font-lock-keyword-face)
    ( ,(regexp-opt cqp-builtins 'words) . font-lock-builtin-face)
    )
  "Keyword highlighting specification for `cqp-mode'.")


;;;###autoload
(define-derived-mode cqp-mode prog-mode "cqp"
  "A major mode for editing CQP files."
  :syntax-table cqp-mode-syntax-table
  (setq-local comment-start "#")
  (setq-local comment-start-skip "#+\\s-*")
  (setq-local font-lock-defaults
              '((cqp-font-lock-keywords))))

;;;###autoload
(define-derived-mode cqp-result-mode text-mode "cqp-result"
  "A major mode for viewing CQP results."
  (setq-local font-lock-defaults `(((,(rx bol (* blank) (1+ (not (any blank digit))) (0+ any) eol) . font-lock-comment-face)
                                    (,(rx "<" (1+ char) ">") . font-lock-keyword-face)
                                    (,(rx "@" (1+ (any alnum "_"))) . font-lock-variable-name-face)
                                    (,(rx "#" (1+ alnum)) . font-lock-reference-face)))))

(provide 'cqp)
 ;;; cqp.el ends here
