
("nixfiles" "org" "rcs" "shell")

(("nixfiles" (:foreground "royal blue") "*.nix" nil nil nil nil) ("org" (:foreground "magenta") "*.org" nil nil nil nil) ("rcs" (:foreground "coral") ".*rc" nil nil nil nil) ("shell" (:weight semi-bold :foreground "goldenrod") "*.sh" nil nil nil nil))
