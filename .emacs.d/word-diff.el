(require 'rx)
(require 'diff-mode)

(define-derived-mode word-diff-mode text-mode "word-diff-mode"
  "A major mode for viewing git diff --word-diff patches"
  (setq-local font-lock-defaults `(((,(rx (group-n 1 "[-") (group-n 2 (1+ (not (any ?- ?\])))) (group-n 3 "-]")) (1 diff-indicator-removed-face) (2 'diff-removed) (3 diff-indicator-removed-face))
                                    (,(rx (group-n 1 "{+") (group-n 2 (1+ (not (any ?+ ?})))) (group-n 3 "+}")) (1 diff-indicator-added-face) (2 'diff-added) (3 diff-indicator-added-face))
                                    (,(rx "@@ " (1+ (not (any ?@))) " @@") . diff-hunk-header)))))
