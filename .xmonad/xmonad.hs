import XMonad
import XMonad.Actions.CycleWS
import XMonad.Actions.Search
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.OnPropertyChange
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops ( ewmh )

import XMonad.Layout.PerWorkspace
import XMonad.Layout.Tabbed

-- Promts
import XMonad.Prompt
import XMonad.Prompt.Pass
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Shell
import XMonad.Prompt.Theme
import XMonad.Prompt.Window
import XMonad.Prompt.Workspace

import XMonad.Util.Run ( safeSpawn, spawnPipe )
import XMonad.Util.SpawnOnce ( spawnOnce, spawnOnOnce )

import XMonad.Util.EZConfig ( additionalKeys, additionalKeysP )
import XMonad.Util.Paste (sendKeyWindow)
import XMonad.Util.Themes


import System.IO
import qualified XMonad.StackSet as W

import Control.Monad
import Data.List

import Data.Foldable

import qualified Data.Map as M

import qualified DBus as D
import qualified DBus.Client as D

import qualified Codec.Binary.UTF8.String as UTF8


myXPConfig = def { searchPredicate = fuzzyMatch
                   --, sorter = fuzzySort
                 , historyFilter = deleteConsecutive
                 , promptKeymap = emacsLikeXPKeymap
                 , font = "xft:Source Sans Pro:style=Light:pixelsize=17"
                 , borderColor = "chartreuse"
                 , bgHLight = "chartreuse"
                 , position = CenteredAt 0.3 0.5
                 , height = 25
                 , maxComplRows = return 15
                 }

withHistory hist = let c = myXPConfig
  in
  c { promptKeymap = M.union (M.fromList [ ((0,xK_Up), historyUpMatching hist)
                                   , ((0,xK_Down), historyDownMatching hist)
                                   ]) (promptKeymap c)
    }

newtype HMenu = HMenu String

instance XPrompt HMenu where
  showXPrompt (HMenu k) = k <> ": "
  completionToCommand _ = id

hmenu :: HMenu -> [String] -> (String -> X u) -> X (Maybe u)
hmenu key ts cnt = do
  hist <- initMatches
  let cfg = withHistory hist
  mkXPromptWithReturn key cfg (mkComplFunFromList' cfg ts) cnt

flameshot = spawn "flameshot gui"

pongsurf = do
  tunnelPID <- spawnPID "ssh -N -D 9000 pong"
  spawn $ "http_proxy=\"socks5://localhost:9000\" surf && kill " <> show tunnelPID

-- pulseaudio controls
pulseRaise sink = spawn $ "pactl set-sink-volume " ++ sink ++ " +5%"
pulseLower sink = spawn $ "pactl set-sink-volume " ++ sink ++ " -5%"
pulseMute sink = spawn $ "pactl set-sink-mute " ++ sink ++ " toggle"

withDefaultSink :: (String -> X ()) -> X ()
withDefaultSink cnt = cnt "@DEFAULT_SINK@"

pulseMuteMic source = spawn $ "pactl set-source-mute " ++ source ++ " toggle"

withDefaultSource :: (String -> X ()) -> X ()
withDefaultSource cnt = cnt "@DEFAULT_SOURCE@"

playerCmd = spawn . ((<>) "dbus-send --print-reply --dest=$(dbus-send --session --dest=org.freedesktop.DBus --type=method_call --print-reply /org/freedesktop/DBus org.freedesktop.DBus.ListNames | grep \"org.mpris.MediaPlayer2.\" | cut -d '\"' -f 2) /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.")
-- playback controls
sendPlayPause = playerCmd "PlayPause"
sendNext = playerCmd "Next"
sendPrev = playerCmd "Previous"

--other helper functions
lockScreen = spawn "slock"

finder = spawn "rifle $(locate workspace | rofi -threads 0 -dmenu -i -p \"locate:\")"
literatureRoots = [ "/literature/*"
                  --, "/workspace/RANT/literature/*"
                  ]
joinCmds cmds = "{ " ++ intercalate " & " cmds ++ " }"
literature = spawn $ "rifle $( " ++ allLiterature ++ " | rofi -threads 0 -dmenu -i -p \"literature:\")"
  where
    allLiterature = joinCmds $ ("ls " ++) <$> literatureRoots

showPass = passPrompt . withHistory =<< initMatches
showRunner = shellPrompt . withHistory =<< initMatches
showTheme = themePrompt . withHistory =<< initMatches
showWindows = windowPrompt myXPConfig Goto allWindows
showWorkspaces = workspacePrompt myXPConfig $ windows . W.view

screenMenu = spawn $ "echo \"" ++ unlines xrandrCmds ++ "\" | rofi -threads 0 -dmenu | $0"
  where
    xrandrCmds = ("xrandr " ++) <$> [ "--auto"
                                    , "--output HDMI1 --mode 1920x1200 --left-of eDP1"
                                    ]

specialWorkspaces = [ ((["<XF86Search>", "<XF86Bluetooth>", "<F10>"], "e"), "work") -- e for emacs
                    , ((["<XF86LaunchA>", "<F11>"], "t"), "term")
                    , ((["<XF86Explorer>", "<F12>"], "b"), "www") -- b for browser
                    , ((["<XF86Tools>", "<F9>"], "s"), "social")
                    , ((["<XF86Display>", "<F7>"], "m"), "music")
                    ]

normalWorkspaces = show <$> [1..9]

customWS ((kcs, ok), worksp) =
  ((\kc -> [("S-" ++ kc, windows $ W.shift worksp), (kc, toggleOrView worksp)]) =<< kcs) ++
  [ ("M-c " ++ ok, toggleOrView worksp)
  , ("S-M-c " ++ ok, windows $ W.shift worksp)
  ]

normalWS w = [ ("M-c " ++ w, toggleOrView w)
             , ("S-M-c " ++ w, windows $ W.shift w)
             ]

dbusLogIF = "org.xmonad.Log"

-- Emit a DBus signal on log updates
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ dbusLogIF
    memberName = D.memberName_ "Update"

myLogHook :: D.Client -> PP
myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    , ppCurrent = wrap "%{B#504945} " " %{B-}"
    , ppVisible = wrap "%{B#3c3836} " " %{B-}"
    , ppUrgent = wrap "%{F#fb4934} " " %{F-}"
    , ppHidden = wrap " " " "
    , ppWsSep = ""
    , ppSep = " : "
    , ppTitle = shorten 40
    }

myTabbed = tabbed shrinkText (theme darkTheme)

brighter = spawn "light -A 5"
darker = spawn "light -U 5"

doKill :: ManageHook
doKill = (ask >>= liftX . killWindow) >> doF id

manageZoomHook =
  composeAll $
    [ (className =? zoomClassName) <&&> shouldFloat <$> title --> doFloat,
      (className =? zoomClassName) <&&> shouldSink <$> title --> doSink
    ]
  where
    zoomClassName = "zoom"
    tileTitles =
      [ "Zoom - Free Account", -- main window
        "Zoom - Licensed Account", -- main window
        "Zoom Workspace - Licensed Account",
        "Zoom Workplace - Licensed account",
        "Zoom Workspace - Free Account",
        "Zoom Workspace - Free account",
        "Zoom", -- meeting window on creation
        "Zoom Meeting", -- meeting window shortly after creation
        "Meeting"
      ]
    shouldFloat title = title `notElem` tileTitles
    shouldSink title = title `elem` tileTitles
    doSink = (ask >>= doF . W.sink) <+> doF W.swapDown

main = do
  -- Polybar communication over DBus
  dbus <- D.connectSession
  -- Request access to the DBus name
  D.requestName dbus (D.busName_ dbusLogIF) [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
  xmonad $ ewmh $ docks $ def
    { layoutHook = avoidStruts $ onWorkspace "social" myTabbed $ layoutHook def
    , logHook = dynamicLogWithPP (myLogHook dbus)
    , manageHook = manageZoomHook 
                     <+> (className =? "DBeaver" <&&> title =? "Tip of the day " --> doKill) -- Kill annoying DBeaver popup
                     <+> manageHook def
    , handleEventHook = onTitleChange manageZoomHook 
                          <+> handleEventHook def 
    , startupHook = do
        setWMName "LG3D" -- had something to do with java guis...
        spawnOnce "systemctl --user start polybar"
    , modMask    = modKey
    , terminal   = "wezterm"
    , workspaces = normalWorkspaces ++ (snd <$> specialWorkspaces)
    } `additionalKeysP` (
    [ ("<XF86AudioLowerVolume>", withDefaultSink pulseLower)
    , ("<XF86AudioRaiseVolume>", withDefaultSink pulseRaise)
    , ("<XF86MonBrightnessUp>",  brighter)
    , ("<XF86MonBrightnessDown>",darker)
    , ("<Print>", flameshot)
    , ("<XF86AudioPlay>", sendPlayPause)
    , ("M-m m", sendPlayPause)
    , ("M-m <Space>", sendPlayPause)
    , ("<XF86AudioPrev>", sendPrev)
    , ("M-m <L>", sendPrev)
    , ("<XF86AudioNext>", sendNext)
    , ("M-m <R>", sendNext)
    , ("<F2>", withDefaultSink pulseLower)
    , ("<F3>", withDefaultSink pulseRaise)
    , ("<F6>", brighter)
    , ("<F5>", darker)
    , ("M-x p", showPass)
    , ("M-x l", lockScreen)
    , ("M-x m m", muteMic)
    , ("<XF86AudioMicMute>", muteMic) -- short shortcut for laptop keyboard
    , ("<F4>", muteMic)
    , ("M-x m s", muteSpeaker)
    , ("<XF86AudioMute>", muteSpeaker) -- short shortcut for laptop keyboard
    , ("<F1>", muteSpeaker) -- short shortcut for laptop keyboard
    , ("M-x x", showRunner)
    , ("M-x t", showTheme)
    , ("M-d", showRunner) -- preserve default keybinding in case I forget
    , ("M-b", pongsurf)
    , ("M-x w", showWorkspaces)
    , ("M-x b", showWindows)
    , ("M-x f f", finder)
    , ("M-x f l", literature)
    , ("M-x M-c", kill)
    , ("M-x s", screenMenu)
    ]
    ++ (normalWS =<< normalWorkspaces)
    ++ (customWS =<< specialWorkspaces))
    where
      muteSpeaker = withDefaultSink pulseMute
      muteMic = withDefaultSource pulseMuteMic
      modKey = mod4Mask -- Super as mod key
