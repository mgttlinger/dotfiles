# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'param: %d'
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents pwd
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._- ,]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' menu select=3
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/merlin/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh-hist
HISTSIZE=3000
SAVEHIST=100000000000
setopt appendhistory autocd extendedglob nomatch
# End of lines configured by zsh-newuser-install

autoload -U colors
colors

bindkey -e

if [ -n "${commands[sk-share]}" ]; then
    source $(sk-share)/completion.zsh
    source $(sk-share)/key-bindings.zsh
elif [ -n "${commands[fzf-share]}" ]; then
    source $(fzf-share)/key-bindings.zsh
    source $(fzf-share)/completion.zsh
fi


if [ -n "${commands[direnv]}" ]; then
    eval "$(direnv hook zsh)"
fi


dot () {
    case "$1" in
        git)
            pushd "/workspace/dotfiles/"
            git "${@:2}"
            popd
            ;;
        *)
            command dot "$@"
            ;;
    esac
}

docs () {
    case "$1" in
        git)
            pushd "/workspace/documents/"
            git "${@:2}"
            popd
            ;;
        *)
            command docs "$@"
            ;;
    esac
}

lit () {
    case "$1" in
        git)
            pushd "/literature/"
            git "${@:2}"
            popd
            ;;
        gadd)
            pushd "/literature/"
            git add "*.pdf"
            git add "*.epub"
            git add "*.djvu"
            popd
            ;;
        *)
            command lit "$@"
            ;;
    esac
}

function vterm_printf(){
    if [ -n "$TMUX" ]; then
        # tell tmux to pass the escape sequences through
        # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'
fi

export HH_CONFIG=hicolor        # get more colors

export CLICOLOR=1

# custom tools before system tools

export SBT_OPTS="-XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=2G -Xmx2G"
export JAVA_OPTS="-Xmx2G"


clear
