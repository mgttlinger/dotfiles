#! /bin/sh

cp -r .config ~/.config
cp .emacs ~/.emacs
cp -r .git ~/.git
cp -r .i3 ~/.i3
cp .vimrc ~/.vimrc
cp -r .xmonad ~/.xmonad
cp -r .zsh ~/.zsh
cp .zshrc ~/.zshrc
